1.  Create a single HTML document named “index.html”.

2.  Inside this file, create the basic structure of a web page including the appropriate "html", "head", "meta", and "body" tags.

3.  Inside the document "head", include the appropriate "title" tag and "meta" tags.

4.  Include the title (“Assignment #X – LastName, FirstName”) at the top of the page using a "h1" tag.  This must be centered.

5.  Create  the  following  sections  and  their  appropriate  contents.   Each  section  should  consists  of  a  single "div" tag (all content for that section should be within the "div" tag).  Each section should be labeled
with an appropriate heading.  The heading must be inside an appropriate heading tag.  The sections that must be included are detailed below.  Please feel free to be creative (there is no need for this to be REAL information). Be sure to include lists, etc.

a) Summary: This section should include a basic biography and any interesting facts about yourself that you’d like to include.
b) Personal Information: This should include name, address, phone number, etc.
c)Academic Information: List all of your high school and college information including any particular notes about classes that you have taken. You may include future education that you hope to have aswell.
d) Employment Information: List any employment past, present, or future.